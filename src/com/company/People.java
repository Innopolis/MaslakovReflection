package com.company;

import java.io.Serializable;

/**
 * Created by sa on 10.02.17.
 */
public class People implements Serializable{
    private String name;
    private Integer age;
    private Double salary;

    protected void paySalary(){
        System.out.println("I have salary " + salary);
    }

    public People(String name, Integer age, double salary) {
        this.name = name;
        this.age = age;
        this.salary = salary;
    }

    public People(){

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public double getSalary() {
        return salary;
    }

    public void setSalary(double salary) {
        this.salary = salary;
    }
}
