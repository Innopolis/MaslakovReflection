package com.company;

import org.w3c.dom.DOMImplementation;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;
import xmlLogic.XmlWriterClass;


import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.Field;

public class Main {

	public static void main(String[] args) throws NoSuchMethodException, ParserConfigurationException, IllegalAccessException, NoSuchFieldException, IOException, SAXException, TransformerException, ClassNotFoundException, InstantiationException {
		People people = new People("vasa", 1, 28.5);

		people.getClass().getDeclaredFields();

		people.getClass().getMethods();

		XmlWriterClass xmlWriterClass = new XmlWriterClass(people);
		xmlWriterClass.writeFile("class.xml");
		People newPeople = (People) xmlWriterClass.readFile("class.xml");
		System.out.println(newPeople.getName());
	}
}

